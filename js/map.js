// LEAFLET MAP INITIALIZATION
  var map = L.map('map').setView([40.6617125, -97.1047804], 3);
  var data = {};
  var iconAdtlr = L.icon({
      iconUrl: 'img/adtlr-icon.png',
      iconSize: [44, 44],
      popupAnchor: [0, -20],
      iconAnchor: [20, 41]
  });

  var defaultIcon = L.icon({
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon-2x.png',
    iconSize: [25,41],
    popupAnchor: [0, -20],
    iconAnchor: [10, 41]
  });

// map initialization and settings
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidmFzaWxocmlzdG9mZiIsImEiOiJjaWp4dXd3Z3MxYzZqdmJsemo0aGM0MDg4In0.tdnsJBxxRwlkF1s42xx4MQ', {
      maxZoom: 15,
      id: 'mapbox.satellite'
  }).addTo(map);


  $.getJSON('js/data/data.json', initData);

  function loadLocationsOnMap(data) {
    var elementStyle = "font-size:15px;font-family:Quicksand,sans-serif;font-weight:700";
    for (var i = 0; i < data.length; i++) {
      L.marker([data[i].longtitude, data[i].lattitude]).addTo(map).bindPopup('<h3 style='+ elementStyle +'><i class="ion-person" style="padding-right:.5em;font-size:17px;  "></i>'+ data[i].city +'</h3><i style="display:none" class="state_id">'+ data[i].id +'</i>');
    }
  }


  function loadDefaultLocation(element,data) {
    var elementStyle = "font-size:15px;font-family:Quicksand,sans-serif;font-weight:700";
    L.marker([data[element].longtitude, data[element].lattitude], {icon: iconAdtlr}).addTo(map).bindPopup('<h3 style='+ elementStyle +'><i class="ion-person" style="padding-right:.5em;font-size:17px;  "></i>'+ data[element].city +'</h3><i style="display:none" class="state_id">'+ data[element].id +'</i>').openPopup();
    outputStateData(element+1, data);
  }


  function outputStateData (id,states) {
    var currentCity = $('.current-city');
    var tableLocation = $('#tableLocation');
    var tableLocationRow ;
    $('.data-container').removeClass('animated fadeInUp')

    var selectedLocation = states[id-1];
    var locationRows = []
    for (var property in selectedLocation) {
        if (selectedLocation.hasOwnProperty(property)) {
          var comparrisons = (selectedLocation['ethnicity'] === 'Hispanic or Latino' && property  === 'ethnicity') ||
                             (selectedLocation['zip'] === 98001 && property ===  'zip') ||
                             (selectedLocation['physicalInactivity'] === 1 && property ===  'physicalInactivity');

          // var ribbonDiv = '<div class="ui black ribbon label tiny">Hotel</div>';                   
          var hightLightClass = comparrisons ? 'positive' : '';
          // var addRibbon = comparrisons ? '<h1>Test</h1>' : '';

          locationRow = '<tr class="'+ hightLightClass +'">' +  
              '<td style="font-size:13px;"><b>'+ property +'</b></td>' +
              '<td class="right aligned" style="padding-right:.5em;">' +
                selectedLocation[property] +
              '</td>' +
            '</tr>';

            locationRows.push(locationRow); 
        }
    }

    tableLocation.find('tbody').html('');
    tableLocation.find('tbody').append(locationRows)
    currentCity.html(states[id-1].city);
    setTimeout(function() {
        $('.data-container').addClass('animated fadeInUp');
    }, 10);
  }


  //  -- PARAMETERS : element , animationType, src , single element
  //  animations removed from outputData function 
  function changeContent(id,data,activeTemplateClass) {
    var activeTemplateId = activeTemplateClass.replace('template-','')
    var ethnicity = data[id-1].ethnicity;

    if (activeTemplateId === '1') { 
      var section = $('.' + activeTemplateClass);
      section.find('h1').find('.ethnicity-text').removeClass('animated fadeInRight')
      section.find('.wireframe').first().removeClass('animated fadeInRight');
      
      setTimeout(function() {
        // if (ethnicity == 'Asian') {
          var textContent = ethnicity;
          section.find('h1').find('.ethnicity-text').html(textContent).addClass('animated fadeInRight')
          switch (ethnicity) {
            case 'Asian':
              section.find('.wireframe').first().attr('src', '../img/asian.jpg').addClass('animated fadeInRight');
              break;

            case 'Hispanic or Latino':
              section.find('.wireframe').first().attr('src', '../img/hispanic.jpg').addClass('animated fadeInRight');
              break;
            
            case 'White' :
              section.find('.wireframe').first().attr('src', '../img/white.jpg').addClass('animated fadeInRight');
              break;
            
            case 'African American' :
              section.find('.wireframe').first().attr('src', '../img/african_american.jpg').addClass('animated fadeInRight');
              break;
            
            case 'Native American' :
              section.find('.wireframe').first().attr('src', '../img/native_american.jpg').addClass('animated fadeInRight');
              break;
          }
          // }
      }, 10);
    }

  }      
  


  function initData(data) {
    loadLocationsOnMap(data);
    loadDefaultLocation(0 ,data);

    // change icons
    map.on('popupclose', function(e) {
      var currentPopup =  e.popup._source;
      currentPopup.setIcon(defaultIcon);
    });

    map.on('popupopen', function(e) {
      var popupOpenedIconSource = e.popup._source;
      popupOpenedIconSource.setIcon(iconAdtlr);
      var elementID = parseInt($(e.popup._source._popup._content).eq(1).html());
      outputStateData(elementID,data);

      var sectionClasses = $('[class*=template-]').attr('class').split(' ');
      var activeTemplateClass = sectionClasses[sectionClasses.length - 1];
      changeContent(elementID, data, activeTemplateClass);
    });
  }
