// --- RESIZE NAVBAR

var containerWidth ;
var leftCol = $('.left-col').width();
$('.fixed.menu').css('left' , leftCol);



// --- STEP BY STEP GUIDE

function triggerInfoSteps() {
  var tour = new Shepherd.Tour({
    defaults: {
      classes: 'shepherd-theme-arrows'
    },
    
  });

  tour.addStep('example1', {
    title: 'Map',
    text: 'Explanations for the map...',
    attachTo: '.map-container right',
    advanceOn: '.data-container click'
  });

  tour.addStep('example2', {
    title: 'Map Data',
    text: 'Explanations for the data that is loaded...',
    attachTo: '.data-container right',
    advanceOn: 'tr.positive'
  });

 step3 =  tour.addStep('example3', {
    title: 'Map Data',
    text: 'Explanations for the data that is loaded...',
    attachTo: 'tr.positive right',
    advanceOn: '.menu.fixed'
  });

  tour.addStep('example4', {
    title: 'Top Menu',
    text: 'Explanations for the top menu ...',
    attachTo: '.menu.fixed bottom',
    advanceOn: '#ajaxContainer'
  });

  tour.addStep('example5', {
    title: 'Main content',
    text: 'Alabala Portokala. Tajnamajna...',
    attachTo: '#ajaxContainer left',
    advanceOn: ''
  });

  tour.start();
  
  localStorage.setItem('stepByStep', false);

  step3.on('show', function (e) {
    if (e.step.id === 'example3') { 
      $('.shepherd-active .data-container').css('opacity', '1')
    } else {
      $('.shepherd-active .data-container').css('opacity', '.3')      
    }
  })

  tour.on('complete', function () {
    $('.data-container').css('opacity', '1')
  })
}

var stepByStepExists = localStorage.getItem('stepByStep');

if (!stepByStepExists) {
  triggerInfoSteps();
  $('.shepherd-active .data-container').css('opacity', '.3');
  $('.data-container').removeClass('animated fadeInUp');     
}

$('.guide-btn').on('click' , function () {
  $('.data-container').removeClass('animated fadeInUp');
  triggerInfoSteps();
})



// --- LOAD SECTIONS

function loadSection(sectionId) {
  var loadingHtml = '<div class="ui active inverted dimmer loading-animation-container"><div class="ui medium text loader">Loading</div></div>';
  $.ajax({
       url: "../templates/template"+ sectionId + ".html", dataType: "html",
       beforeSend: function () {
          setTimeout(function () {
            loadContentPlaceholder.prepend(loadingHtml)
          },10)
       }
  }).done(function( responseHtml ) {
       loadContentPlaceholder.html(responseHtml);
      setTimeout(function () {
         $('.loading-animation-container').remove();
      },500)
  });
}

var navItem = $('.nav-sections').find('.item');
var loadContentPlaceholder = $("#ajaxContainer");
navItem.on('click', function (e) {
  e.preventDefault();
  $('.nav-sections').find('.item').removeClass('active')
  $(this).addClass('active');
  var sectionId = $(this).attr('id').replace('template-','');
  loadSection(sectionId);
})



// load first section on page load
setTimeout(function() {
  loadSection(1);
}, 1);
